package ru.ekfedorov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.model.Session;
import ru.ekfedorov.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @NotNull
    @WebMethod
    Task addTask(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "description", partName = "description") @NotNull String description
    );

    @Nullable
    @WebMethod
    Task bindTaskByProject(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String projectId,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String taskId
    );

    @Nullable
    Task changeTaskStatusById(
            @NotNull Session session,
            @NotNull String id,
            @NotNull Status status
    );

    @Nullable
    Task changeTaskStatusByIndex(
            @NotNull Session session,
            @NotNull Integer index,
            @NotNull Status status
    );

    @Nullable
    Task changeTaskStatusByName(
            @NotNull Session session,
            @NotNull String name,
            @NotNull Status status
    );

    @NotNull
    @WebMethod
    List<Task> findAllByProjectId(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String projectId
    );

    @NotNull
    @WebMethod
    List<Task> findAllTask(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    );

    @NotNull
    @WebMethod
    List<Task> findTaskAllWithComparator(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "sort", partName = "sort") @NotNull String sort
    );

    @Nullable
    @WebMethod
    Task findTaskOneById(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    );

    @Nullable
    @WebMethod
    Task findTaskOneByIndex(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    );

    @Nullable
    @WebMethod
    Task findTaskOneByName(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    );

    @Nullable
    Task finishTaskById(
            @NotNull Session session,
            @NotNull String id
    );

    @Nullable
    Task finishTaskByIndex(
            @NotNull Session session,
            @NotNull Integer index
    );

    @Nullable
    Task finishTaskByName(
            @NotNull Session session,
            @NotNull String name
    );

    @WebMethod
    boolean removeTask(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "task", partName = "task") @NotNull Task task
    );

    @WebMethod
    boolean removeTaskOneById(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    );

    @WebMethod
    boolean removeTaskOneByIndex(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    );

    @WebMethod
    boolean removeTaskOneByName(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    );

    @Nullable
    Task startTaskById(
            @NotNull Session session,
            @NotNull String id
    );

    @Nullable
    Task startTaskByIndex(
            @NotNull Session session,
            @NotNull Integer index
    );

    @Nullable
    Task startTaskByName(
            @NotNull Session session,
            @NotNull String name
    );

    @Nullable
    @WebMethod
    Task unbindTaskFromProject(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String taskId
    );

    @Nullable
    Task updateTaskById(
            @NotNull Session session,
            @NotNull String id,
            @NotNull String name,
            @NotNull String description
    );

    @Nullable
    Task updateTaskByIndex(
            @NotNull Session session,
            @NotNull Integer index,
            @NotNull String name,
            @NotNull String description
    );

}
