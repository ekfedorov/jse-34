package ru.ekfedorov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.enumerated.Role;
import ru.ekfedorov.tm.model.Session;
import ru.ekfedorov.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IAdminUserEndpoint {

    @WebMethod
    void clearUser(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    );

    @WebMethod
    void createUserWithRole(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password,
            @WebParam(name = "role", partName = "role") @Nullable Role role
    );

    @NotNull
    @WebMethod
    List<User> findAllUser(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    );

    @Nullable
    @WebMethod
    User findUserOneById(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    );

    @WebMethod
    void lockUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    );

    @WebMethod
    void removeByLogin(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    );

    @WebMethod
    User removeUserOneById(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    );

    @WebMethod
    void unlockUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    );

}
