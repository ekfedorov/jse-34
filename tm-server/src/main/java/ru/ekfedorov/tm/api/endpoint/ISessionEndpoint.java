package ru.ekfedorov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ISessionEndpoint {

    @Nullable
    @WebMethod
    Session closeSession(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    );

    @WebMethod
    List<Session> listSession();

    @WebMethod
    Session openSession(
            @WebParam(name = "login", partName = "login") String login,
            @WebParam(name = "password", partName = "password") String password
    );

}
