package ru.ekfedorov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.model.Session;
import ru.ekfedorov.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IUserEndpoint {

    @WebMethod
    void createUser(
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password,
            @WebParam(name = "email", partName = "email") @Nullable String email
    );

    @WebMethod
    User findUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    );

    @Nullable
    @WebMethod
    User findUserOneBySession(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    );

    @WebMethod
    void setPassword(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "password", partName = "password") @Nullable String password
    );

    @WebMethod
    void userUpdate(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "firstName", partName = "firstName") @Nullable String firstName,
            @WebParam(name = "lastName", partName = "lastName") @Nullable String lastName,
            @WebParam(name = "middleName", partName = "middleName") @Nullable String middleName
    );

}
