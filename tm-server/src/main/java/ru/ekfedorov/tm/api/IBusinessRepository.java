package ru.ekfedorov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IBusinessRepository<E extends AbstractBusinessEntity> extends IRepository<E> {

    void clear(@NotNull String userId);

    @NotNull
    List<E> findAll(@Nullable String userId, @NotNull Comparator<E> comparator);

    @NotNull
    List<E> findAll(@Nullable String userId);

    @NotNull
    Optional<E> findOneById(@Nullable String userId, @NotNull String id);

    @NotNull
    Optional<E> findOneByIndex(@Nullable String userId, @NotNull Integer index);

    @NotNull
    Optional<E> findOneByName(@Nullable String userId, @NotNull String name);

    boolean remove(@Nullable String userId, @NotNull E entity);

    boolean removeOneById(@Nullable String userId, @NotNull String id);

    boolean removeOneByIndex(
            @Nullable String userId, @NotNull Integer index
    );

    boolean removeOneByName(
            @Nullable String userId, @NotNull String name
    );

}
