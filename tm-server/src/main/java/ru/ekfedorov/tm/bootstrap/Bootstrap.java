package ru.ekfedorov.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.endpoint.*;
import ru.ekfedorov.tm.api.repository.*;
import ru.ekfedorov.tm.api.service.*;
import ru.ekfedorov.tm.component.Backup;
import ru.ekfedorov.tm.endpoint.*;
import ru.ekfedorov.tm.enumerated.Role;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.model.Session;
import ru.ekfedorov.tm.repository.*;
import ru.ekfedorov.tm.service.*;
import ru.ekfedorov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
@Setter
@NoArgsConstructor
public class Bootstrap implements ServiceLocator {

    @NotNull
    public final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    public final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    public final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    public final IUserRepository userRepository = new UserRepository();

    @NotNull
    public final IBackupService backupService = new BackupService(this);

    @NotNull
    public final ILoggerService loggerService = new LoggerService();

    @NotNull
    public final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    public final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    public final IPropertyService propertyService = new PropertyService();

    @NotNull
    public final ISessionService sessionService = new SessionService(this, sessionRepository);

    @NotNull
    public final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    public final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    public final IAdminEndpoint adminEndpoint = new AdminEndpoint(this);

    @NotNull
    public final IAdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(this);

    @NotNull
    public final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    public final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @NotNull
    public final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    public final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @Nullable
    private Session session = null;

    public void init() {
        initPID();
        initUser();
        initDefaultData();
        initBackup();
        initEndpoint();
    }

    private void initBackup() {
        backup.init();
    }

    private void initDefaultData() {
        final String userId = userRepository.findByLogin("test").get().getId();
        projectService.add(userId, "bbb", "-").setStatus(Status.IN_PROGRESS);
        projectService.add(userId, "bab", "-").setStatus(Status.COMPLETE);
        projectService.add(userId, "aaa", "-").setStatus(Status.NOT_STARTED);
        projectService.add(userId, "ccc", "-").setStatus(Status.COMPLETE);
        taskService.add(userId, "bbb", "-").setStatus(Status.IN_PROGRESS);
        taskService.add(userId, "aaa", "-").setStatus(Status.NOT_STARTED);
        taskService.add(userId, "ccc", "-").setStatus(Status.COMPLETE);
        taskService.add(userId, "eee", "-").setStatus(Status.NOT_STARTED);
        taskService.add(userId, "ddd", "-").setStatus(Status.IN_PROGRESS);
    }

    private void initEndpoint() {
        registry(sessionEndpoint);
        registry(taskEndpoint);
        registry(projectEndpoint);
        registry(userEndpoint);
        registry(adminUserEndpoint);
        registry(adminEndpoint);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initUser() {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

}
