package ru.ekfedorov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.endpoint.EndpointLocator;
import ru.ekfedorov.tm.api.service.ServiceLocator;
import ru.ekfedorov.tm.bootstrap.Bootstrap;
import ru.ekfedorov.tm.endpoint.Session;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public abstract class AbstractCommand {

    @Nullable
    protected ServiceLocator serviceLocator;

    @Nullable
    protected EndpointLocator endpointLocator;

    @Nullable
    protected Bootstrap bootstrap;

    @Nullable
    public abstract String commandArg();

    @Nullable
    public abstract String commandDescription();

    @Nullable
    public abstract String commandName();

    public abstract void execute();

    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public void setEndpointLocator(@NotNull final EndpointLocator endpointLocator) {
        this.endpointLocator = endpointLocator;
    }

    public void setBootstrap(@Nullable final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @NotNull
    @Override
    public String toString() {
        String result = "";
        if (!isEmpty(commandName())) result += commandName();
        if (!isEmpty(commandArg())) result += " [" + commandArg() + "]";
        if (!isEmpty(commandDescription())) result += " - " + commandDescription();
        return result;
    }

}
