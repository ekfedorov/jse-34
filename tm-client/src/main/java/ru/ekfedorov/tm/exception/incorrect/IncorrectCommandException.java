package ru.ekfedorov.tm.exception.incorrect;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.exception.AbstractException;

public final class IncorrectCommandException extends AbstractException {

    public IncorrectCommandException(@NotNull final String command) throws Exception {
        super("Error! Command ``" + command + "`` not found...");
    }

}
