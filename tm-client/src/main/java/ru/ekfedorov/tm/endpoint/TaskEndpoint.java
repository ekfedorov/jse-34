package ru.ekfedorov.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.4.2
 * 2021-03-10T08:04:56.442+03:00
 * Generated source version: 3.4.2
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.ekfedorov.ru/", name = "TaskEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface TaskEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/unbindTaskFromProjectRequest", output = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/unbindTaskFromProjectResponse")
    @RequestWrapper(localName = "unbindTaskFromProject", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.UnbindTaskFromProject")
    @ResponseWrapper(localName = "unbindTaskFromProjectResponse", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.UnbindTaskFromProjectResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.ekfedorov.tm.endpoint.Task unbindTaskFromProject(

        @WebParam(name = "session", targetNamespace = "")
        ru.ekfedorov.tm.endpoint.Session session,
        @WebParam(name = "taskId", targetNamespace = "")
        java.lang.String taskId
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/startTaskByNameRequest", output = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/startTaskByNameResponse")
    @RequestWrapper(localName = "startTaskByName", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.StartTaskByName")
    @ResponseWrapper(localName = "startTaskByNameResponse", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.StartTaskByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.ekfedorov.tm.endpoint.Task startTaskByName(

        @WebParam(name = "session", targetNamespace = "")
        ru.ekfedorov.tm.endpoint.Session session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/startTaskByIdRequest", output = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/startTaskByIdResponse")
    @RequestWrapper(localName = "startTaskById", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.StartTaskById")
    @ResponseWrapper(localName = "startTaskByIdResponse", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.StartTaskByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.ekfedorov.tm.endpoint.Task startTaskById(

        @WebParam(name = "session", targetNamespace = "")
        ru.ekfedorov.tm.endpoint.Session session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/changeTaskStatusByIdRequest", output = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/changeTaskStatusByIdResponse")
    @RequestWrapper(localName = "changeTaskStatusById", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.ChangeTaskStatusById")
    @ResponseWrapper(localName = "changeTaskStatusByIdResponse", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.ChangeTaskStatusByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.ekfedorov.tm.endpoint.Task changeTaskStatusById(

        @WebParam(name = "session", targetNamespace = "")
        ru.ekfedorov.tm.endpoint.Session session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id,
        @WebParam(name = "status", targetNamespace = "")
        ru.ekfedorov.tm.endpoint.Status status
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/finishTaskByNameRequest", output = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/finishTaskByNameResponse")
    @RequestWrapper(localName = "finishTaskByName", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.FinishTaskByName")
    @ResponseWrapper(localName = "finishTaskByNameResponse", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.FinishTaskByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.ekfedorov.tm.endpoint.Task finishTaskByName(

        @WebParam(name = "session", targetNamespace = "")
        ru.ekfedorov.tm.endpoint.Session session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/startTaskByIndexRequest", output = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/startTaskByIndexResponse")
    @RequestWrapper(localName = "startTaskByIndex", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.StartTaskByIndex")
    @ResponseWrapper(localName = "startTaskByIndexResponse", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.StartTaskByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.ekfedorov.tm.endpoint.Task startTaskByIndex(

        @WebParam(name = "session", targetNamespace = "")
        ru.ekfedorov.tm.endpoint.Session session,
        @WebParam(name = "index", targetNamespace = "")
        java.lang.Integer index
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/findAllByProjectIdRequest", output = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/findAllByProjectIdResponse")
    @RequestWrapper(localName = "findAllByProjectId", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.FindAllByProjectId")
    @ResponseWrapper(localName = "findAllByProjectIdResponse", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.FindAllByProjectIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.ekfedorov.tm.endpoint.Task> findAllByProjectId(

        @WebParam(name = "session", targetNamespace = "")
        ru.ekfedorov.tm.endpoint.Session session,
        @WebParam(name = "projectId", targetNamespace = "")
        java.lang.String projectId
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/finishTaskByIndexRequest", output = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/finishTaskByIndexResponse")
    @RequestWrapper(localName = "finishTaskByIndex", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.FinishTaskByIndex")
    @ResponseWrapper(localName = "finishTaskByIndexResponse", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.FinishTaskByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.ekfedorov.tm.endpoint.Task finishTaskByIndex(

        @WebParam(name = "session", targetNamespace = "")
        ru.ekfedorov.tm.endpoint.Session session,
        @WebParam(name = "index", targetNamespace = "")
        java.lang.Integer index
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/findAllTaskRequest", output = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/findAllTaskResponse")
    @RequestWrapper(localName = "findAllTask", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.FindAllTask")
    @ResponseWrapper(localName = "findAllTaskResponse", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.FindAllTaskResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.ekfedorov.tm.endpoint.Task> findAllTask(

        @WebParam(name = "session", targetNamespace = "")
        ru.ekfedorov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/findTaskOneByIndexRequest", output = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/findTaskOneByIndexResponse")
    @RequestWrapper(localName = "findTaskOneByIndex", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.FindTaskOneByIndex")
    @ResponseWrapper(localName = "findTaskOneByIndexResponse", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.FindTaskOneByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.ekfedorov.tm.endpoint.Task findTaskOneByIndex(

        @WebParam(name = "session", targetNamespace = "")
        ru.ekfedorov.tm.endpoint.Session session,
        @WebParam(name = "index", targetNamespace = "")
        java.lang.Integer index
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/findTaskOneByNameRequest", output = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/findTaskOneByNameResponse")
    @RequestWrapper(localName = "findTaskOneByName", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.FindTaskOneByName")
    @ResponseWrapper(localName = "findTaskOneByNameResponse", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.FindTaskOneByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.ekfedorov.tm.endpoint.Task findTaskOneByName(

        @WebParam(name = "session", targetNamespace = "")
        ru.ekfedorov.tm.endpoint.Session session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/finishTaskByIdRequest", output = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/finishTaskByIdResponse")
    @RequestWrapper(localName = "finishTaskById", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.FinishTaskById")
    @ResponseWrapper(localName = "finishTaskByIdResponse", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.FinishTaskByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.ekfedorov.tm.endpoint.Task finishTaskById(

        @WebParam(name = "session", targetNamespace = "")
        ru.ekfedorov.tm.endpoint.Session session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/updateTaskByIdRequest", output = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/updateTaskByIdResponse")
    @RequestWrapper(localName = "updateTaskById", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.UpdateTaskById")
    @ResponseWrapper(localName = "updateTaskByIdResponse", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.UpdateTaskByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.ekfedorov.tm.endpoint.Task updateTaskById(

        @WebParam(name = "session", targetNamespace = "")
        ru.ekfedorov.tm.endpoint.Session session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "description", targetNamespace = "")
        java.lang.String description
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/removeTaskOneByIdRequest", output = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/removeTaskOneByIdResponse")
    @RequestWrapper(localName = "removeTaskOneById", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.RemoveTaskOneById")
    @ResponseWrapper(localName = "removeTaskOneByIdResponse", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.RemoveTaskOneByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean removeTaskOneById(

        @WebParam(name = "session", targetNamespace = "")
        ru.ekfedorov.tm.endpoint.Session session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/updateTaskByIndexRequest", output = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/updateTaskByIndexResponse")
    @RequestWrapper(localName = "updateTaskByIndex", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.UpdateTaskByIndex")
    @ResponseWrapper(localName = "updateTaskByIndexResponse", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.UpdateTaskByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.ekfedorov.tm.endpoint.Task updateTaskByIndex(

        @WebParam(name = "session", targetNamespace = "")
        ru.ekfedorov.tm.endpoint.Session session,
        @WebParam(name = "index", targetNamespace = "")
        java.lang.Integer index,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "description", targetNamespace = "")
        java.lang.String description
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/addTaskRequest", output = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/addTaskResponse")
    @RequestWrapper(localName = "addTask", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.AddTask")
    @ResponseWrapper(localName = "addTaskResponse", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.AddTaskResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.ekfedorov.tm.endpoint.Task addTask(

        @WebParam(name = "session", targetNamespace = "")
        ru.ekfedorov.tm.endpoint.Session session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "description", targetNamespace = "")
        java.lang.String description
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/findTaskAllWithComparatorRequest", output = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/findTaskAllWithComparatorResponse")
    @RequestWrapper(localName = "findTaskAllWithComparator", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.FindTaskAllWithComparator")
    @ResponseWrapper(localName = "findTaskAllWithComparatorResponse", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.FindTaskAllWithComparatorResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.ekfedorov.tm.endpoint.Task> findTaskAllWithComparator(

        @WebParam(name = "session", targetNamespace = "")
        ru.ekfedorov.tm.endpoint.Session session,
        @WebParam(name = "sort", targetNamespace = "")
        java.lang.String sort
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/removeTaskRequest", output = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/removeTaskResponse")
    @RequestWrapper(localName = "removeTask", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.RemoveTask")
    @ResponseWrapper(localName = "removeTaskResponse", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.RemoveTaskResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean removeTask(

        @WebParam(name = "session", targetNamespace = "")
        ru.ekfedorov.tm.endpoint.Session session,
        @WebParam(name = "task", targetNamespace = "")
        ru.ekfedorov.tm.endpoint.Task task
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/changeTaskStatusByIndexRequest", output = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/changeTaskStatusByIndexResponse")
    @RequestWrapper(localName = "changeTaskStatusByIndex", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.ChangeTaskStatusByIndex")
    @ResponseWrapper(localName = "changeTaskStatusByIndexResponse", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.ChangeTaskStatusByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.ekfedorov.tm.endpoint.Task changeTaskStatusByIndex(

        @WebParam(name = "session", targetNamespace = "")
        ru.ekfedorov.tm.endpoint.Session session,
        @WebParam(name = "index", targetNamespace = "")
        java.lang.Integer index,
        @WebParam(name = "status", targetNamespace = "")
        ru.ekfedorov.tm.endpoint.Status status
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/bindTaskByProjectRequest", output = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/bindTaskByProjectResponse")
    @RequestWrapper(localName = "bindTaskByProject", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.BindTaskByProject")
    @ResponseWrapper(localName = "bindTaskByProjectResponse", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.BindTaskByProjectResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.ekfedorov.tm.endpoint.Task bindTaskByProject(

        @WebParam(name = "session", targetNamespace = "")
        ru.ekfedorov.tm.endpoint.Session session,
        @WebParam(name = "projectId", targetNamespace = "")
        java.lang.String projectId,
        @WebParam(name = "taskId", targetNamespace = "")
        java.lang.String taskId
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/removeTaskOneByNameRequest", output = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/removeTaskOneByNameResponse")
    @RequestWrapper(localName = "removeTaskOneByName", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.RemoveTaskOneByName")
    @ResponseWrapper(localName = "removeTaskOneByNameResponse", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.RemoveTaskOneByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean removeTaskOneByName(

        @WebParam(name = "session", targetNamespace = "")
        ru.ekfedorov.tm.endpoint.Session session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/removeTaskOneByIndexRequest", output = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/removeTaskOneByIndexResponse")
    @RequestWrapper(localName = "removeTaskOneByIndex", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.RemoveTaskOneByIndex")
    @ResponseWrapper(localName = "removeTaskOneByIndexResponse", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.RemoveTaskOneByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean removeTaskOneByIndex(

        @WebParam(name = "session", targetNamespace = "")
        ru.ekfedorov.tm.endpoint.Session session,
        @WebParam(name = "index", targetNamespace = "")
        java.lang.Integer index
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/changeTaskStatusByNameRequest", output = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/changeTaskStatusByNameResponse")
    @RequestWrapper(localName = "changeTaskStatusByName", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.ChangeTaskStatusByName")
    @ResponseWrapper(localName = "changeTaskStatusByNameResponse", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.ChangeTaskStatusByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.ekfedorov.tm.endpoint.Task changeTaskStatusByName(

        @WebParam(name = "session", targetNamespace = "")
        ru.ekfedorov.tm.endpoint.Session session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "status", targetNamespace = "")
        ru.ekfedorov.tm.endpoint.Status status
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/findTaskOneByIdRequest", output = "http://endpoint.tm.ekfedorov.ru/TaskEndpoint/findTaskOneByIdResponse")
    @RequestWrapper(localName = "findTaskOneById", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.FindTaskOneById")
    @ResponseWrapper(localName = "findTaskOneByIdResponse", targetNamespace = "http://endpoint.tm.ekfedorov.ru/", className = "ru.ekfedorov.tm.endpoint.FindTaskOneByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.ekfedorov.tm.endpoint.Task findTaskOneById(

        @WebParam(name = "session", targetNamespace = "")
        ru.ekfedorov.tm.endpoint.Session session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id
    );
}
