
package ru.ekfedorov.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.ekfedorov.tm.endpoint package. 
 * &lt;p&gt;An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ClearProject_QNAME = new QName("http://endpoint.tm.ekfedorov.ru/", "clearProject");
    private final static QName _ClearProjectResponse_QNAME = new QName("http://endpoint.tm.ekfedorov.ru/", "clearProjectResponse");
    private final static QName _ClearTask_QNAME = new QName("http://endpoint.tm.ekfedorov.ru/", "clearTask");
    private final static QName _ClearTaskResponse_QNAME = new QName("http://endpoint.tm.ekfedorov.ru/", "clearTaskResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.ekfedorov.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ClearProject }
     * 
     */
    public ClearProject createClearProject() {
        return new ClearProject();
    }

    /**
     * Create an instance of {@link ClearProjectResponse }
     * 
     */
    public ClearProjectResponse createClearProjectResponse() {
        return new ClearProjectResponse();
    }

    /**
     * Create an instance of {@link ClearTask }
     * 
     */
    public ClearTask createClearTask() {
        return new ClearTask();
    }

    /**
     * Create an instance of {@link ClearTaskResponse }
     * 
     */
    public ClearTaskResponse createClearTaskResponse() {
        return new ClearTaskResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearProject }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ClearProject }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ekfedorov.ru/", name = "clearProject")
    public JAXBElement<ClearProject> createClearProject(ClearProject value) {
        return new JAXBElement<ClearProject>(_ClearProject_QNAME, ClearProject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearProjectResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ClearProjectResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ekfedorov.ru/", name = "clearProjectResponse")
    public JAXBElement<ClearProjectResponse> createClearProjectResponse(ClearProjectResponse value) {
        return new JAXBElement<ClearProjectResponse>(_ClearProjectResponse_QNAME, ClearProjectResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearTask }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ClearTask }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ekfedorov.ru/", name = "clearTask")
    public JAXBElement<ClearTask> createClearTask(ClearTask value) {
        return new JAXBElement<ClearTask>(_ClearTask_QNAME, ClearTask.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearTaskResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ClearTaskResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ekfedorov.ru/", name = "clearTaskResponse")
    public JAXBElement<ClearTaskResponse> createClearTaskResponse(ClearTaskResponse value) {
        return new JAXBElement<ClearTaskResponse>(_ClearTaskResponse_QNAME, ClearTaskResponse.class, null, value);
    }

}
